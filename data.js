import * as tf from '@tensorflow/tfjs';

const IMAGEH = 28;
const IMAGEW = 28;
const IMAGESIZE = IMAGEH * IMAGEW;

const MNIST_IMAGES_SPRITE_PATH =
    'https://storage.googleapis.com/learnjs-data/model-builder/mnist_images.png';
const MNIST_LABELS_PATH =
    'https://storage.googleapis.com/learnjs-data/model-builder/mnist_labels_uint8';

const TOTALELEMENTS = 65000;
const TRAINELEMENTS = 50000;

const CLASSES=10; //check on this param
const TESTELEMENTS = TOTALELEMENTS - TRAINELEMENTS;

export class mnistData(){
  async loadData(){
    // create image and canvas
    const image = new Image();
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');

    const imageRequest = new Promise((resolve, reject) => {
        // Writeup image properties and options
        image.crossOrigin = '';
        img.src = MNIST_IMAGES_SPRITE_PATH;
        image.onload = () => {
          image.width = image.naturalWidth;
          image.height = image.naturalHeight;

          // 4 bytes for each char. Also becuase we're using float array
          const datasetArrayBuffer = new ArrayBuffer(TOTALELEMENTS * IMAGESIZE * 4);

          // load image in batches and also images are loaded vertically fitting in the chunksSize.
          //Let's play with this param later
          const chunkSize = 5000;
          canvas.width = image.width;
          canvas.height = chunkSize;

          for (let i = 0; i < NUM_DATASET_ELEMENTS / chunkSize; i++) {
            // float32Array(buffersize, byte offset, length)
            const datasetView = new Float32Array(datasetArrayBuffer,
                i * IMAGESIZE * chunkSize * 4, IMAGESIZE * chunksSize);

            // As we've allocated enough space, let's draw the images
            // void ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
            context.drawImage(image, 0, i * chunkSize, image.width, chunkSize, 0, 0, image.width, chunkSize);

            // Get pixel data from images
            const imageData = context.getImageData(0, 0, canvas.width, canvas.height);

            for (let j = 0; j < imageData.data.length / 4; j++) {
              // All channels hold an equal value since the image is grayscale, so
              // just read the red channel.
              // TODO: try to read any other channel and also figure out how it differs
              // with a color image
              datasetView[j] = imageData.data[j * 4] / 255;
            }
          }
          this.datasetImages = new Float32Array(datasetBytesBuffer);
          resolve();

        }
    });

    const labelsReq = fetch(MNIST_LABELS_PATH);

    //Split images into multiple sets
    this.trainImages = this.datasetImages.slice(0, IMAGESIZE * TRAINELEMENTS);
    // Naturally remaining elements are test imagesets
    this.trainImages = this.datasetImages.slice(0, IMAGESIZE * TRAINELEMENTS);
    this.trainLabels = this.datasetImages.slice(0, CLASSES * TRAINELEMENTS);
    this.testLabels = this.datasetImages.slice(CLASSES * TRAINELEMENTS);

  }

/*
xs is the tensordata
shape is [numberof elements, image H, image W, 1]
*/
getTrainingData(){
  const xs = tf.tensor4d(this.trainImages, [this.testImages.length/ IMAGESIZE, IMAGEH, IMAGEW, 1]);
  const labels = tf.tensor2d(this.trainLabels, [this.trainLabels.length/CLASSES, CLASSES]);

  return {xs, labels};
}

/*
xs is the tensordata
shape is [numberof elements, image H, image W, 1]
*/
getTrainingData(noOfExamples){
  let xs = tf.tensor4d(this.testImages, [this.testImages.length/ IMAGESIZE, IMAGEH, IMAGEW, 1]);
  const labels = tf.tensor2d(this.testLabels, [this.testLabels.length/CLASSES, CLASSES]);

  if(noOfExamples !== null){
    xs = xs.slice([0, 0, 0, 0], [numExamples, IMAGE_H, IMAGE_W, 1]);
    labels = labels.slice([0, 0], [numExamples, NUM_CLASSES]);
  }

  return {xs, labels};
}


}
